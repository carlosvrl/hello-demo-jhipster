package com.cif.cucumber.stepdefs;

import com.cif.HelloDemoJhipsterApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = HelloDemoJhipsterApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
