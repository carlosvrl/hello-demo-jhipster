(function() {
    'use strict';

    angular
        .module('helloDemoJhipsterApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
